ARG apiPort=500
FROM node:12-alpine AS builder
# Add yarn
WORKDIR /app
RUN apk add --update --no-cache python3 make g++ gcc libgcc libstdc++ linux-headers
RUN yarn global add node-gyp --no-cache
# Install dependencies
COPY package.json yarn.lock ./
RUN yarn --frozen-lockfile && \
yarn cache clean
# Build app
COPY . .
RUN yarn build api-auth --prod

FROM node:12-alpine AS runner
WORKDIR /app
RUN apk add --update --no-cache python3 make g++ gcc libgcc libstdc++ linux-headers
COPY package.json yarn.lock ./
COPY --from=builder /app/dist/apps/api/auth .
RUN yarn --frozen-lockfile --production && \
yarn cache clean
EXPOSE ${apiPort}
CMD ["node", "main.js"]
