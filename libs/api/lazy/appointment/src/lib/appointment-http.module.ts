import { Module } from '@nestjs/common';
import { AppointmentHTTPControler } from './infrastructure/api/appointment-http.controller';

@Module({
  controllers: [AppointmentHTTPControler]
})
export class AppointmentHTTPModule {}
