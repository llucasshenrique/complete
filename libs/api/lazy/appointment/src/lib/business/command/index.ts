import { ScheduleAppointmentCommandHandler } from './handler/schedule-appointment.command-handler';

export const AppointmentCommandHandlers = [
    ScheduleAppointmentCommandHandler
]
export * from './schedule-appointment.command'