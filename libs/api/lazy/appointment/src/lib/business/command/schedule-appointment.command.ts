import { ICommand } from '@nestjs/cqrs';

export class ScheduleAppointment implements ICommand {
  constructor(
    public readonly requesterId: string,
    public readonly requestedId: string,
    public readonly schedule: string
  ) { }
}