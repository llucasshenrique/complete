import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { v4 } from 'uuid';
import { ScheduleAppointment } from '../schedule-appointment.command';
import { EntityManager } from 'typeorm';
import { UserEntity } from '../../../infrastructure/repository/entity/User.entity';
import { AppointmentConnection } from '../../../infrastructure/appointment.constant';
import { InjectEntityManager } from '@nestjs/typeorm';

@CommandHandler(ScheduleAppointment)
export class ScheduleAppointmentCommandHandler implements ICommandHandler<ScheduleAppointment> {
  constructor(
    @InjectEntityManager(AppointmentConnection)
    private readonly entityManager: EntityManager,
    private readonly publisher: EventPublisher
  ) { }
  async execute({ requesterId, requestedId, schedule }: ScheduleAppointment) {
    const scheduledId: string = v4()
    const userRepo = this.entityManager.getRepository(UserEntity)
    const requested = await userRepo.findOneOrFail(requestedId)
    const requester = await userRepo.findOneOrFail(requesterId)
    requested.schedules.find((_schedule) => _schedule.id === scheduledId)
    return scheduledId
  }
}
