import { IQuery, QueryHandler } from '@nestjs/cqrs';
import { Query } from '@nestjs/common';

export class DoesRequesterIsAvailable implements IQuery {
    constructor(
        public readonly requesterId: string,
        public readonly schedule: string
    ) {}
}