import { QueryHandler, IQueryHandler } from "@nestjs/cqrs";
import { DoesRequestedScheduleIsAvailable } from '../does-requested-schedule-is-available.query';
import { DoesRequesterIsAvailable } from '../does-requester-is-available.query';

@QueryHandler(DoesRequestedScheduleIsAvailable)
export class DoesRequestedScheduleIsAvailableQueryHandler implements IQueryHandler<DoesRequesterIsAvailable> {
    async execute(query: DoesRequesterIsAvailable): Promise<any> {
        return true
    }

}