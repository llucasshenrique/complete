import { IQueryHandler, QueryHandler } from "@nestjs/cqrs";
import { DoesRequestedScheduleIsAvailable } from '../does-requested-schedule-is-available.query';
import { DoesRequesterIsAvailable } from '../does-requester-is-available.query';

@QueryHandler(DoesRequesterIsAvailable)
export class DoesRequesterIsAvailableQueryHandler implements IQueryHandler<DoesRequestedScheduleIsAvailable> {
    async execute(query: DoesRequestedScheduleIsAvailable): Promise<any> {
        return true
    }
}