import { IQuery } from '@nestjs/cqrs';

export class DoesRequestedScheduleIsAvailable implements IQuery {
    constructor(
        public readonly requestedId: string,
        public readonly schedule: string
    ) {}
}