import { DoesRequestedScheduleIsAvailableQueryHandler } from './handler/does-requested-schedule-is-available.query-handler';
import { DoesRequesterIsAvailableQueryHandler } from './handler/does-requester-is-available.query-handler';

export const AppointmentQueryHandlers = [
    DoesRequesterIsAvailableQueryHandler,
    DoesRequestedScheduleIsAvailableQueryHandler,
]
export * from './does-requested-schedule-is-available.query';
export * from './does-requester-is-available.query';
