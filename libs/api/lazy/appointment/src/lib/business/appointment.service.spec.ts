import { CommandBus, CqrsModule, QueryBus } from '@nestjs/cqrs';
import { Test } from '@nestjs/testing';
import { getRepositoryToken, TypeOrmModule } from '@nestjs/typeorm';
import { ScheduleEntity } from '../infrastructure/repository/entity/schedule.entity';
import { UserEntity } from '../infrastructure/repository/entity/User.entity';
import { AppointmentService } from "./appointment.service";
import { AppointmentCommandHandlers } from './command';
import { AppointmentQueryHandlers } from './query';
import { AppointmentConnection } from '../infrastructure/appointment.constant';

describe('Appointment Service', () => {
  let appointmentService: AppointmentService
  let queryBus: QueryBus
  let commandBus: CommandBus
  jest.mock('uuid/v4', () => () => ('uuid0123-abcd-M123-Nb23-0123456789ab'))

  beforeEach(async () => {
    const testModule = await Test.createTestingModule({
      imports: [
        CqrsModule,
        TypeOrmModule.forRoot({
          name: AppointmentConnection,
          type: 'sqlite',
          database: ':memory:',
          entities: [UserEntity, ScheduleEntity],
          synchronize: true
        })
      ],
      providers: [
        // {
        //   provide: getRepositoryToken(UserEntity, {
        //     name: AppointmentConnection,
        //     type: 'sqlite',
        //     database: ':memory:',
        //     entities: [UserEntity]
        //   }),
        //   useValue: {
        //     findOneOrFail: jest.fn()
        //   }
        // },
        // {
        //   provide: getRepositoryToken(ScheduleEntity, {
        //     name: AppointmentConnection,
        //     type: 'sqlite',
        //     database: ':memory:',
        //     entities: [ScheduleEntity]
        //   }),
        //   useValue: {
        //     findOneOrFail: jest.fn()
        //   }
        // },
        ...AppointmentCommandHandlers,
        ...AppointmentQueryHandlers,
        AppointmentService]
    }).compile()
    commandBus = testModule.get(CommandBus)
    queryBus = testModule.get(QueryBus)
    appointmentService = testModule.get(AppointmentService)
    commandBus.register(AppointmentCommandHandlers)
    queryBus.register(AppointmentQueryHandlers)
  })
  it('Should instantiate', () => {
    expect(appointmentService).toBeDefined()
  })
  it('Should create an request', async () => {
    const result = await appointmentService.scheduleAppointment({ requestedId: '0', requesterId: '1', schedule: '2020-06-11' })
    expect(result).toMatch(/^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i)
  })
})
