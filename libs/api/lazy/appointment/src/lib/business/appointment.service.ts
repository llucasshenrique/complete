import { Injectable } from "@nestjs/common";
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ScheduleAppointment } from './command/schedule-appointment.command';
import { DoesRequestedScheduleIsAvailable } from './query/does-requested-schedule-is-available.query';
import { DoesRequesterIsAvailable } from './query/does-requester-is-available.query';

interface ScheduleRequest {
    requesterId: string;
    requestedId: string;
    schedule: string;
}

@Injectable()
export class AppointmentService {
    constructor(
        private readonly commandBus: CommandBus,
        private readonly queryBus: QueryBus
    ) { }
    async scheduleAppointment({ requesterId, requestedId, schedule }: ScheduleRequest) {
        //  Para que a requisição seja feita com sucesso o horario deve estar disponivel
        await this.assertRequestedScheduleIsAvailable(requestedId, schedule)
        await this.assertRequesterIsAvailable(requesterId, schedule)
        const scheduleId = await this.commandBus.execute(new ScheduleAppointment(requestedId, requestedId, schedule))
        return scheduleId
    }
    private async assertRequesterIsAvailable(requesterId: string, schedule: string) {
        const requesterIsAvailable = await this.queryBus.execute(
            new DoesRequesterIsAvailable(requesterId, schedule)
        )
        if (!requesterIsAvailable) { throw new Error("Method not implemented."); }
    }
    private async assertRequestedScheduleIsAvailable(requestedId: string, schedule: string) {
        const requestedScheduleIsAvailable = await this.queryBus.execute(
            new DoesRequestedScheduleIsAvailable(requestedId, schedule)
        )
        if (!requestedScheduleIsAvailable) { throw new Error("Method not implemented."); }
    }
}