import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppointmentService } from './business/appointment.service';

@Module({
  imports: [
    CqrsModule,
    TypeOrmModule.forFeature([], 'AppointmentConnection')
  ],
  providers: [
    AppointmentService
  ]
})
export class AppointmentModule {}
