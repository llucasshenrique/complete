import { ManyToOne, Column, PrimaryColumn, Entity } from 'typeorm';
import { UserEntity } from './User.entity';

@Entity({ name: 'Schedule' })
export class ScheduleEntity {
  @PrimaryColumn('uuid', { unique: true, nullable: false })
  public id!: string
  @Column('date')
  date: string
  @Column('time')
  time: string
  @ManyToOne(type => UserEntity, user => user.schedules)
  user: UserEntity
}
