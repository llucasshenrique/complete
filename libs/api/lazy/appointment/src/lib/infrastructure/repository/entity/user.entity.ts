import { Column, Entity, OneToMany, PrimaryColumn } from "typeorm";
import { ScheduleEntity } from './schedule.entity';

@Entity({name: 'User'})
export class UserEntity {
    @PrimaryColumn('uuid', {unique: true, nullable: false})
    public id!: string
    @Column()
    public accountId: string
    @OneToMany(type => ScheduleEntity, schedule => schedule.user )
    schedules: ScheduleEntity[]
}
