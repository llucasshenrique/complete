import { Body, Controller, Post } from "@nestjs/common";
import { ClientProxy, ClientProxyFactory, Transport } from '@nestjs/microservices';
import { ApiConflictResponse, ApiCreatedResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('appointment')
@Controller('appointment')
export class AppointmentHTTPControler {
  private client: ClientProxy
  constructor() {
    this.client = ClientProxyFactory.create({
      transport: Transport.TCP
    })
  }
  @Post('scheduleAppointment')
  @ApiCreatedResponse({
    description: 'The Schedule had been reserved'
  })
  @ApiConflictResponse({
    description: 'The schedule cannot be reserved'
  })
  scheduleAppointment(@Body() dto: any) {
    return this.client
    .send<any,any>({ cmd: 'scheduleAppointment'}, dto)
  }
}
