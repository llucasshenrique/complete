import { Module } from '@nestjs/common';
import {AuthHTTPController} from './infrastructure/api/auth-http.controller'
@Module({
  controllers: [AuthHTTPController]
})
export class AuthHTTPModule {}
