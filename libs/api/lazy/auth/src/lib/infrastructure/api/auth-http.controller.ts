import { Body, Controller, HttpException, HttpStatus, Post } from '@nestjs/common'
import { ClientProxy, ClientProxyFactory, Transport } from '@nestjs/microservices'
import { ApiBadRequestResponse, ApiConflictResponse, ApiCreatedResponse, ApiTags } from '@nestjs/swagger'
import { Observable, of } from 'rxjs'
import { catchError } from 'rxjs/operators'
import { AuthClearTextCredentialsDto } from '../../domain/dto/auth-credentials.dto'
import { ChangePasswordDto } from '../../domain/dto/change-password.dto'

interface AuthCommand {
  register(dto: AuthClearTextCredentialsDto): Observable<any>
  login(dto: AuthClearTextCredentialsDto): Observable<any>
}

@ApiTags('auth')
@Controller('auth')
export class AuthHTTPController {
  private client: ClientProxy
  constructor() {
    this.client = ClientProxyFactory.create({
      transport: Transport.TCP
    })
  }
  @Post('register')
  @ApiCreatedResponse({
    description: 'The account has been successfully created'
  })
  @ApiBadRequestResponse({
    description:
      'The format of the body is incorrect. We need an valid email and password'
  })
  @ApiConflictResponse({
    description: 'The email already has been registered. Try login instead.'
  })
  @ApiConflictResponse({
    description:
      'We are suffering problems to fulfill this request, please try again later.'
  })
  register(@Body() dto: AuthClearTextCredentialsDto) {
    return this.client
      .send<any, AuthClearTextCredentialsDto>({ cmd: 'RegisterUser' }, dto)
      .pipe(
        catchError(error => {
          if (error.code === 'WEAKPASSWORD')
            return of(
              new HttpException(
                `The providede password is too weak.
                Provide one password at minimal 8 characters long with at least on Uppcase letter, one lowercase letter, one number and one expecial character.`,
                HttpStatus.BAD_REQUEST
              )
            )
          if (error.code === 'EMAILALREADYREG')
            return of(
              new HttpException(
                'The email provided has already been registered, please try login instead.',
                HttpStatus.BAD_REQUEST
              )
            )
          if (error.code === 'ECONNREFUSED')
            return of(
              new HttpException(
                'We are having problems to fulfill this request, please try again later.',
                HttpStatus.INTERNAL_SERVER_ERROR
              )
            )
        })
      )
  }
  @Post('login')
  @ApiBadRequestResponse({    description: 'Wrong credentials'  })
  login(@Body() dto: AuthClearTextCredentialsDto) {
    return this.client.send<any, AuthClearTextCredentialsDto>(
      {
        cmd: 'LoginUser'
      },
      dto
    )
  }
  @Post('changepassword')
  changePassword( @Body() dto: ChangePasswordDto) {
    return this.client
    .send<any,  ChangePasswordDto >(
      { cmd: 'ChangePassword' },
      dto
    )
  }
}
