import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { AuthService } from '../../business/auth.service';
import { AuthClearTextCredentialsDto } from '../../domain/dto/auth-credentials.dto';
import { ChangePasswordDto } from '../../domain/dto/change-password.dto'

@Controller()
export class AuthTCPController {
  constructor(private readonly authService: AuthService) {}
  @MessagePattern({ cmd: 'RegisterUser' })
  registerUser(payload: AuthClearTextCredentialsDto) {
    return this.authService.register(payload);
  }
  @MessagePattern({ cmd: 'LoginUser' })
  loginUser(payload: AuthClearTextCredentialsDto) {
    return this.authService.login(payload);
  }
  @MessagePattern({cmd: 'ChangePassword'})
  changePassword(payload: ChangePasswordDto) {
    return this.authService.changePassword(payload)
  }
}
