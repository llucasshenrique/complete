import { Entity, PrimaryColumn, Column } from 'typeorm'

@Entity({ name: 'event' })
export class EventEntity {
  @PrimaryColumn('uuid')
  id: string
  @Column('integer')
  lastReadEvent: number
}
