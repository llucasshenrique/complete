import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TCPClient } from 'geteventstore-promise';
import { from } from 'rxjs';
import { Repository } from 'typeorm';
import { AccountConnection, TCPClientToken } from './auth-app.config';
import { AccountEntity } from './infrastructure/repository/entity/account.entity';
import { EventEntity } from './infrastructure/repository/entity/event.entity';

@Injectable()
export class StartupService implements OnModuleInit {
  constructor(
    @InjectRepository(AccountEntity, AccountConnection)
    private accountRepository: Repository<AccountEntity>,
    @InjectRepository(EventEntity, AccountConnection)
    private eventRepository: Repository<EventEntity>,
    @Inject(TCPClientToken)
    private eventstore: TCPClient
  ) {}
  async onModuleInit() {
    this.eventstore.checkStreamExists('$ce-account').then(accStreamExists => {
      if (!accStreamExists) {
        return false;
      }
      from(
        this.eventstore.getEventsByType(
          '$ce-account',
          ['AccountRegistered'],
          0,
          100
        )
      )
        .pipe()
        .subscribe();
      // const account = this.accountRepository.create({
      //   email: 'test@email.com',
      //   hashedPassword: '00111',
      //   id: '01',
      //   createdAt: new Date('31/12/2000 14:50:55')
      // })
      // this.accountRepository.save(account)
      return;
    });
    // const quantity = 100;
    // const start = 0;
    // from(
    //   this.eventstore.getEventsByType(
    //     '$ce-account',
    //     ['AccountRegistered'],
    //     start,
    //     quantity
    //   )
    // )
    //   .pipe(
    //     map(events => events.map(({ streamId }) => streamId)),
    //     flatMap(ids => ids.map(id => this.eventstore.getEvents(id))),
    //     map(eventsPromise =>
    //       eventsPromise.then(events =>
    //         events.map(async event => {
    //           const { id, hashedPassword } = event.data as any;
    //           if (event.eventType === 'AccountRegistered') {
    //             await this.accountRepository.save({
    //               ...(event.data as AccountEntity)
    //             });
    //             await this.eventRepository.save({
    //               id: (event.data as any).id,
    //               lastReadEvent: event.eventNumber
    //             });
    //           }
    //           if (event.eventType === 'PasswordChanged') {
    //             await this.accountRepository.findOneOrFail({ id });
    //             await this.accountRepository.update({ id }, { hashedPassword });
    //             await this.eventRepository.update(
    //               { id },
    //               { lastReadEvent: event.eventNumber }
    //             );
    //           }
    //           (event as any).read = true;
    //         })
    //       )
    //     )
    //   )
    //   .subscribe();
  }
}
