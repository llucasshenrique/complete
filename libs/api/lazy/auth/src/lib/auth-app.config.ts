import { TCPConfig } from 'geteventstore-promise'

export abstract class AuthAppConfig {
    production: boolean
    authJsonWebTokenSecredt: string
    microserviceConfig: {
        transport: number
    }
    tcpConfig: TCPConfig
}
export const AccountConnection = 'AccountConnection'
export const AuthAppConfigToken = 'AuthAppConfigToken'
export const TCPClientToken = 'TCPClientToken'
export const TcpClientConfig = 'TcpClientConfig'