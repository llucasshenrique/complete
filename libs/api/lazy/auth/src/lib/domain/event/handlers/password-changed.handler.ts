import { Inject, Logger } from '@nestjs/common'
import { EventsHandler, IEventHandler } from '@nestjs/cqrs'
import { TCPClient } from 'geteventstore-promise'
import { PasswordChanged } from '../password-changed.event'

@EventsHandler(PasswordChanged)
export class PasswordChangedHandler
  implements IEventHandler<PasswordChanged> {
  constructor(
    @Inject(`TCPClientToken`)
    private readonly eventStore: TCPClient
  ) { }
  handle(event: PasswordChanged) {
    Logger.log(event, event.constructor.name)
    return this.eventStore.writeEvent(
      `account-${event.uuid}`,
      event.constructor.name,
      { id: event.uuid, hashedPassword: event.hashedPassword }
    ).then(e => Logger.log(e, event.constructor.name))
      .catch(e => Logger.error(e, event.constructor.name))
  }
}
