export class UserLoggedIn {
  constructor(
    public readonly uuid: string,
    public readonly authorizationToken: string,
    public readonly refreshToken: string,
    public readonly loggedAt: string
  ) {}
}
