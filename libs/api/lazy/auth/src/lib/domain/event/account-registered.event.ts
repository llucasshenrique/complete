import { AuthHashedCredentialsDto } from '../dto/auth-credentials.dto';

export class AccountRegistered{
    constructor(
        public readonly uuid: string,
        public readonly authHashedCredentials: AuthHashedCredentialsDto
    ){}
}