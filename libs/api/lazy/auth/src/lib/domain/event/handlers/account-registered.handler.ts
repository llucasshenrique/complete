import { Inject, Logger } from '@nestjs/common';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { TCPClient } from 'geteventstore-promise';
import { AccountRegistered } from '../account-registered.event';
@EventsHandler(AccountRegistered)
export class AccountRegisteredHandler
  implements IEventHandler<AccountRegistered> {
  constructor(
    @Inject(`TCPClientToken`)
    private readonly eventStore: TCPClient
  ) {}
  handle(event: AccountRegistered) {
    Logger.log(event, event.constructor.name);
    return this.eventStore.writeEvent(
      `account-${event.uuid}`,
      event.constructor.name,
      { id: event.uuid, ...event.authHashedCredentials }
    ).then(e => Logger.log(e, event.constructor.name))
    .catch(e => Logger.error(e, event.constructor.name))
  }
}
