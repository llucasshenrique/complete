export class PasswordChanged {
  constructor(
    public readonly uuid: string,
    public readonly hashedPassword: string
    ) { }
}