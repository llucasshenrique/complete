import { AccountRegisteredHandler } from './handlers/account-registered.handler';
import { UserLoggedInHandler } from './handlers/user-loggedin.handler'
import { PasswordChangedHandler } from './handlers/password-changed.handler'

export const AuthEventHandlers = [
    AccountRegisteredHandler,
    UserLoggedInHandler,
    PasswordChangedHandler
]