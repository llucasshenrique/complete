import { Inject, Logger } from '@nestjs/common'
import { EventsHandler, IEventHandler } from '@nestjs/cqrs'
import { TCPClient } from 'geteventstore-promise'
import { UserLoggedIn } from '../user-loggedin.event'

@EventsHandler(UserLoggedIn)
export class UserLoggedInHandler
  implements IEventHandler<UserLoggedIn> {
  constructor(
    @Inject(`TCPClientToken`)
    private readonly eventStore: TCPClient
  ) { }
  handle(event: UserLoggedIn) {
    Logger.log(event, event.constructor.name)
    return this.eventStore.writeEvent(
      `account-${event.uuid}`,
      event.constructor.name,
      {
        id: event.uuid,
        refreshToken: event.refreshToken,
        authorizationToken: event.authorizationToken,
        loggedAt: event.loggedAt
      }
    ).then(e => Logger.log(e, event.constructor.name))
      .catch(e => Logger.error(e, event.constructor.name))
  }
}
