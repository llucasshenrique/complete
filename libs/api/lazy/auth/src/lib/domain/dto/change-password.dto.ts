import { IsString, IsDefined, IsEmail } from 'class-validator'
import { ApiProperty } from '@nestjs/swagger'

export class ChangePasswordDto {
  @ApiProperty()
  @IsDefined()
  @IsEmail()
  email!: string
  @ApiProperty()
  @IsDefined()
  @IsString()
  oldPassword!: string
  @ApiProperty()
  @IsDefined()
  @IsString()
  newPassword!: string
}