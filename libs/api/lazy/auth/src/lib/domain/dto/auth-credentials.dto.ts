import { IsDefined, IsEmail, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger'

export class AuthClearTextCredentialsDto {
    @ApiProperty()
    @IsDefined()
    @IsEmail()
    public readonly email!: string
    @ApiProperty()
    @IsDefined()
    @IsString()
    public readonly password!: string
}
export class AuthHashedCredentialsDto {
    @ApiProperty()
    @IsDefined()
    @IsEmail()
    public readonly email!: string
    @ApiProperty()
    @IsDefined()
    @IsString()
    public readonly hashedPassword!: string
}
