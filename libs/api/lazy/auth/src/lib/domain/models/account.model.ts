import { AggregateRoot } from '@nestjs/cqrs'
import { AuthHashedCredentialsDto } from '../dto/auth-credentials.dto'
import { AccountRegistered } from '../event/account-registered.event'
import { UserLoggedIn } from '../event/user-loggedin.event'
import { PasswordChanged } from '../event/password-changed.event'
import {formatISO} from 'date-fns'
export class Account extends AggregateRoot {
    constructor(private readonly uuid: string) { super() }
    login(authorizationToken: string,refreshToken: string) {
        this.apply(new UserLoggedIn(this.uuid,authorizationToken, refreshToken, formatISO(new Date())))
    }
    register(authHashedCredentialsDto: AuthHashedCredentialsDto) {
        this.apply(new AccountRegistered(this.uuid, authHashedCredentialsDto))
    }
    changePassword(hashedPassword: any) {
        this.apply(new PasswordChanged(this.uuid, hashedPassword))
    }
}
