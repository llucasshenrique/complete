import { Logger } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken, TypeOrmModule } from '@nestjs/typeorm';
import { TCPClient } from 'geteventstore-promise';
import { Repository } from 'typeorm';
import { AccountConnection, TCPClientToken } from './auth-app.config';
import { AccountEntity } from './infrastructure/repository/entity/account.entity';
import { EventEntity } from './infrastructure/repository/entity/event.entity';
import { StartupService } from './startup.service';

describe('StartupService', () => {
  let service: StartupService;
  let accountRepo: Repository<AccountEntity>;
  let accountRepoSpy;
  let eventRepo: Repository<EventEntity>;
  let esTCPClient: TCPClient;
  let esClientSpy;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        StartupService,
        {
          provide: getRepositoryToken(AccountEntity, {
            name: AccountConnection,
            type: 'sqlite',
            database: ':memory:',
            entities: [AccountEntity],
            synchronize: true
          }),
          useValue: {
            findOneOrFail: jest.fn()
          }
        },
        {
          provide: getRepositoryToken(EventEntity, {
            name: AccountConnection,
            type: 'sqlite',
            database: ':memory:',
            entities: [EventEntity],
            synchronize: true
          }),
          useValue: {}
        },
        {
          provide: TCPClientToken,
          useValue: {
            checkStreamExists: async () => jest.fn(() => true),
            getEventsByType: async () => jest.fn()
          }
        }
      ]
    }).compile();

    service = module.get<StartupService>(StartupService);
    accountRepo = module.get<Repository<AccountEntity>>(
      getRepositoryToken(AccountEntity, AccountConnection)
    );
    eventRepo = module.get<Repository<EventEntity>>(
      getRepositoryToken(EventEntity, AccountConnection)
    );
    esTCPClient = module.get<TCPClient>(TCPClientToken);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return false if no event exists on Stream', async () => {
    esTCPClient.checkStreamExists = jest.fn(async () => false);
    esClientSpy = jest.spyOn(esTCPClient, 'checkStreamExists');
    const returned = await service.onModuleInit();
    expect(esClientSpy).toBeCalledWith('$ce-account');
    expect(returned).toBeFalsy();
  });
  it('should return if events exists on Stream', async () => {
    esClientSpy = jest.spyOn(esTCPClient, 'checkStreamExists');
    const returned = await service.onModuleInit();
    expect(esClientSpy).toBeCalledWith('$ce-account');
    expect(returned).toBeUndefined();
  });
  it('should get all accounts created', async () => {
    esClientSpy = jest.spyOn(esTCPClient, 'getEventsByType');
    await service.onModuleInit();
    expect(esClientSpy).toBeCalledWith(
      '$ce-account',
      ['AccountRegistered'],
      0,
      100
    );
  });
  it('should recreate account if AccountCreated event exists', async () => {
    // accountRepoSpy = jest.spyOn(accountRepo, 'findOneOrFail')
    service.onModuleInit().then(() => {
      const account = accountRepo.findOneOrFail('01');
      expect(account).toBe({});
    });
  });
});
