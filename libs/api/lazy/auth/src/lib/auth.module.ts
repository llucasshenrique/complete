import { Module } from '@nestjs/common'
import { CqrsModule } from '@nestjs/cqrs'
import { TypeOrmModule } from '@nestjs/typeorm'
import { TCPClient } from 'geteventstore-promise'
import { AccountConnection, AuthAppConfig, TcpClientConfig, TCPClientToken } from './auth-app.config'
import { AuthService } from './business/auth.service'
import { AuthCommandHandlers } from './business/command'
import { AuthQueryHandlers } from './business/query'
import { AuthSagas } from './business/sagas/update-query.saga'
import { AuthEventHandlers } from './domain/event'
import { AuthTCPController } from './infrastructure/api/auth-tcp.controller'
import { AccountEntity } from './infrastructure/repository/entity/account.entity'
import { EventEntity } from './infrastructure/repository/entity/event.entity'
import { StartupService } from './startup.service'

function tcpClientFactory(config) {
  return new TCPClient(config)
}

@Module({
  imports: [
    CqrsModule,
    TypeOrmModule.forFeature([
      AccountEntity, EventEntity
    ], AccountConnection)
  ],
  providers: [
    ...AuthCommandHandlers,
    ...AuthEventHandlers,
    ...AuthQueryHandlers,
    {
      provide: TCPClientToken,
      useFactory: tcpClientFactory,
      inject: [TcpClientConfig]
    },
    AuthSagas,
    StartupService,
    AuthService
  ],
  controllers: [AuthTCPController]
})
export class AuthModule {
  static forRoot(authConfig: AuthAppConfig) {
    return {
      module: AuthModule,
      providers: [
        { provide: AuthAppConfig, useValue: authConfig },
        { provide: TcpClientConfig, useValue: authConfig.tcpConfig }
      ]
    }
  }
}
