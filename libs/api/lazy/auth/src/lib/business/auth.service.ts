import { Injectable } from '@nestjs/common'
import { CommandBus, QueryBus } from '@nestjs/cqrs'
import { RpcException } from '@nestjs/microservices'
import { AuthClearTextCredentialsDto } from '../domain/dto/auth-credentials.dto'
import { LoginCommand } from './command/login.command'
import { ChangePasswordCommand } from './command/change-password.command'
import { RegisterAccountCommand } from './command/register-account.command'
import { DoesEmailExistsQuery } from './query/interface/does-email-exists.query'
import { GetUserIdAfterValidationQuery } from './query/interface/get-userid-after-validation.query'
import { ChangePasswordDto } from '../domain/dto/change-password.dto'

@Injectable()
export class AuthService {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus
  ) {
  }
  async login(authCredentials: AuthClearTextCredentialsDto) {
    const id = await this.queryBus.execute<
      GetUserIdAfterValidationQuery,
      string
    >(new GetUserIdAfterValidationQuery(authCredentials))
    const { authorizationToken, refreshToken } = await this.commandBus.execute(
      new LoginCommand(id)
    )
    return { authorizationToken, refreshToken, id }
  }

  async register(authCredentials: AuthClearTextCredentialsDto) {
    await this.assertEmailDoesNotExists(authCredentials.email)
    this.assertPasswordIsStrong(authCredentials.password)
    const id = await this.commandBus.execute(
      new RegisterAccountCommand(authCredentials)
    )
    return { id }
  }
  async changePassword({ email, newPassword, oldPassword }: ChangePasswordDto) {
    const id = await this.queryBus.execute<
      GetUserIdAfterValidationQuery,
      string
    >(new GetUserIdAfterValidationQuery({ email, password: oldPassword }))
    this.assertPasswordIsStrong(newPassword)
    const accepted = await this.commandBus.execute(
      new ChangePasswordCommand(id, newPassword)
    )
    return accepted
  }
  assertPasswordIsStrong(password: string) {
    if (password.length < 8)
      throw new RpcException({
        code: 'WEAKPASSWORD',
        message:
          'The providede password is too weak, provide one password at minimal 8 characters long with at least on Uppcase letter, one lowercase lette, one number and one expecial character.'
      })
    const regExp = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%&*()]).{8,}/
    const isValid = regExp.test(password)
    if (!isValid)
      throw new RpcException({
        code: 'WEAKPASSWORD',
        message:
          'The providede password is too weak, provide one password at minimal 8 characters long with at least on Uppcase letter, one lowercase lette, one number and one expecial character.'
      })
  }
  async assertEmailDoesNotExists(email: string) {
    const emailAlreadyExist = await this.queryBus.execute(
      new DoesEmailExistsQuery(email)
    )
    if (emailAlreadyExist) {
      throw new RpcException({
        code: 'EMAILALREADYREG',
        message: 'Email already registered'
      })
    }
  }
}
