import { Inject } from '@nestjs/common'
import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs'
import { InjectEntityManager } from '@nestjs/typeorm'
import * as crypto from 'crypto'
import jsonwebtoken from 'jsonwebtoken'
import { EntityManager } from 'typeorm'
import { AccountConnection, AuthAppConfig } from '../../../auth-app.config'
import { Account } from '../../../domain/models/account.model'
import { AccountEntity } from '../../../infrastructure/repository/entity/account.entity'
import { LoginCommand } from '../login.command'

@CommandHandler(LoginCommand)
export class LoginHandler implements ICommandHandler<LoginCommand> {
  constructor(
    @Inject(AuthAppConfig)
    private appConfig: AuthAppConfig,
    @InjectEntityManager(AccountConnection)
    private readonly entityManager: EntityManager,
    private readonly publisher: EventPublisher
  ) { }
  async execute({ accountId }: LoginCommand) {
    const accountRepo = this.entityManager.getRepository(AccountEntity)
    const account = await accountRepo.findOneOrFail(accountId)
    const refreshToken = crypto.randomBytes(25).toString('base64')
    const user = this.publisher.mergeObjectContext(new Account(account.id))
    const authorizationToken = jsonwebtoken.sign({ id: accountId }, this.appConfig.authJsonWebTokenSecredt, { notBefore: '-10s', expiresIn: '10min' })
    user.login(authorizationToken, refreshToken)
    user.commit()
    return { refreshToken, authorizationToken, id: accountId }
  }
}
