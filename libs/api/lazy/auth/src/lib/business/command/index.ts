import { LoginHandler } from './handler/login.handler'
import { RegisterAccountHandler } from './handler/register-account.handler'
import { ChangePasswordHandler } from './handler/change-password.handler'

export const AuthCommandHandlers = [RegisterAccountHandler, LoginHandler, ChangePasswordHandler]
