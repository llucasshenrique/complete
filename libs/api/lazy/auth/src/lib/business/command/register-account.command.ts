import { AuthClearTextCredentialsDto } from '../../domain/dto/auth-credentials.dto';

export class RegisterAccountCommand {
  constructor(
    public readonly authCredentialsDto: AuthClearTextCredentialsDto
  ) {}
}
