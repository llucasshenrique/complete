import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs'
import { InjectEntityManager } from '@nestjs/typeorm'
import * as argon2 from 'argon2'
import { EntityManager } from 'typeorm'
import { AccountConnection } from '../../../auth-app.config'
import { Account } from '../../../domain/models/account.model'
import { AccountEntity } from '../../../infrastructure/repository/entity/account.entity'
import { ChangePasswordCommand } from '../change-password.command'

@CommandHandler(ChangePasswordCommand)
export class ChangePasswordHandler implements ICommandHandler<ChangePasswordCommand> {
  constructor(
    @InjectEntityManager(AccountConnection)
    private readonly entityManager: EntityManager,
    private readonly publisher: EventPublisher) { }
  async execute(command: ChangePasswordCommand) {
    const { newPassword, id } = command
    const accountRepo = this.entityManager.getRepository(AccountEntity)
    const account = await accountRepo.findOneOrFail(id)
    const hashedPassword = await argon2.hash(newPassword)
    const user = this.publisher.mergeObjectContext(new Account(account.id))
    account.hashedPassword = hashedPassword
    accountRepo.update({ id }, { hashedPassword })
    user.changePassword(hashedPassword)
    user.commit()
    return
  }
}
