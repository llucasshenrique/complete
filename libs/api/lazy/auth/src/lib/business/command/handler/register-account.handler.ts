import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import * as argon2 from 'argon2';
import uuid from 'uuid';
import { Account } from '../../../domain/models/account.model';
import { RegisterAccountCommand } from '../register-account.command';

@CommandHandler(RegisterAccountCommand)
export class RegisterAccountHandler
  implements ICommandHandler<RegisterAccountCommand> {
      constructor(private readonly publisher: EventPublisher) {}
  async execute(command: RegisterAccountCommand) {
    const {
      authCredentialsDto: { email, password }
    } = command;
    const hashedPassword = await argon2.hash(password)
    const accountId = uuid.v4()
    const user = this.publisher.mergeObjectContext(new Account(accountId))
    user.register({email, hashedPassword})
    user.commit()
    return accountId
  }
}
