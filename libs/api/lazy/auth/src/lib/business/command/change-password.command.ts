import { ChangePasswordDto } from '../../domain/dto/change-password.dto'

export class ChangePasswordCommand {
   constructor(
      public readonly id: string,
      public readonly newPassword: string) { }
}