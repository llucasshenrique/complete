import { Injectable } from '@nestjs/common';
import { InjectEntityManager } from '@nestjs/typeorm';
import { EntityManager } from 'typeorm';
import { Saga, ofType } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AccountRegistered } from '../../domain/event/account-registered.event';
import { AccountEntity } from '../../infrastructure/repository/entity/account.entity';
import { AccountConnection } from '../../auth-app.config'

@Injectable()
export class AuthSagas {
  constructor(
    @InjectEntityManager(AccountConnection)
    private readonly entityManager: EntityManager
  ) {}
  @Saga()
  accountRegistred = (events$: Observable<any>): Observable<any> =>
    events$.pipe(
      ofType(AccountRegistered),
      map(event => {
        const accountRepo = this.entityManager.getRepository(AccountEntity);
        accountRepo.save({
          id: event.uuid,
          email: event.authHashedCredentials.email,
          hashedPassword: event.authHashedCredentials.hashedPassword
        });
      })
    );
}
