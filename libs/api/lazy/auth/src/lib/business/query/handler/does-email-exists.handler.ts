import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { InjectEntityManager } from '@nestjs/typeorm';
import { EntityManager } from 'typeorm';
import { AccountConnection } from '../../../auth-app.config';
import { AccountEntity } from '../../../infrastructure/repository/entity/account.entity';
import { DoesEmailExistsQuery } from '../interface/does-email-exists.query';

@QueryHandler(DoesEmailExistsQuery)
export class DoesEmailExistsHandler
  implements IQueryHandler<DoesEmailExistsQuery> {
  constructor(
    @InjectEntityManager(AccountConnection)
    private readonly entityManager: EntityManager
  ) {}
  async execute(query: DoesEmailExistsQuery): Promise<any> {
    const accountRepo = this.entityManager.getRepository(AccountEntity);
    return (await accountRepo.count({ email: query.email })) > 0;
  }
}
