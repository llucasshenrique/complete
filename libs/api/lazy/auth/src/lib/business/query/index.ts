import { DoesEmailExistsHandler } from './handler/does-email-exists.handler';
import { GetUserIdAfterValidationHandler } from './handler/get-userid-after-validation.handler';
import { GetEventsFromAllAccountsStreamHandler } from './handler/get-events-from-all-accounts-stream.handler';

export const AuthQueryHandlers = [
    DoesEmailExistsHandler,
    GetUserIdAfterValidationHandler,
    GetEventsFromAllAccountsStreamHandler
]