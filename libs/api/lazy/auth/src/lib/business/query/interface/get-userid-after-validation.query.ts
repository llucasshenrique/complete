import { AuthClearTextCredentialsDto } from '../../../domain/dto/auth-credentials.dto';

export class GetUserIdAfterValidationQuery {
  constructor(
    public readonly authCredentialsDto: AuthClearTextCredentialsDto
  ) {}
}
