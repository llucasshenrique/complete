import { IQueryHandler, QueryHandler } from '@nestjs/cqrs'
import { RpcException } from '@nestjs/microservices'
import { InjectEntityManager } from '@nestjs/typeorm'
import * as argon2 from 'argon2'
import { EntityManager } from 'typeorm'
import { AccountConnection } from '../../../auth-app.config'
import { AccountEntity } from '../../../infrastructure/repository/entity/account.entity'
import { GetUserIdAfterValidationQuery } from '../interface/get-userid-after-validation.query'

@QueryHandler(GetUserIdAfterValidationQuery)
export class GetUserIdAfterValidationHandler
  implements IQueryHandler<GetUserIdAfterValidationQuery> {
  constructor(
    @InjectEntityManager(AccountConnection)
    private readonly entityManager: EntityManager
  ) { }
  async execute(query: GetUserIdAfterValidationQuery) {
    const accountRepo = this.entityManager.getRepository(AccountEntity)
    const accountWithMatchedEmail = await accountRepo.findOne({
      email: query.authCredentialsDto.email
    })
    if (!accountWithMatchedEmail)
      throw new RpcException({
        code: 'ACCOUNTNOTFOUND',
        message: 'No account was found with given email.'
      })
    const hashedPassword = accountWithMatchedEmail.hashedPassword
    const validPassword = await argon2.verify(
      hashedPassword,
      query.authCredentialsDto.password
    )
    if (!validPassword)
      throw new RpcException({
        code: 'INVALIDPASSWORD',
        message: 'Invalid password'
      })
    return accountWithMatchedEmail.id
  }
}
