import { CqrsModule, QueryBus } from '@nestjs/cqrs';
import { Test, TestingModule } from '@nestjs/testing';
import { Event, TCPClient } from 'geteventstore-promise';
import { TCPClientToken } from '../../../auth-app.config';
import { GetEventsFromAllAccountsStreamQuery } from '../interface/get-events-from-all-accounts-stream.query';
import { GetEventsFromAllAccountsStreamHandler } from './get-events-from-all-accounts-stream.handler';

describe('Get events from all accounts stream', () => {
  let queryBus: QueryBus;
  let esClient: TCPClient;
  const testEvents: Event[] = [
    {
      streamId: '1',
      eventId: 'a-1',
      eventType: 'AccountCreated',
      eventNumber: 1,
      created: '13/04/2020 11:53:50',
      data: {}
    },
    {
      streamId: '1',
      eventId: 'a-2',
      eventType: 'AccountCreated',
      eventNumber: 1,
      created: '13/04/2020 11:53:53',
      data: {}
    },
    {
      streamId: '1',
      eventId: 'b-1',
      eventType: 'AccountCreated',
      eventNumber: 1,
      created: '13/04/2020 11:53:54',
      data: {}
    },
    {
      streamId: '1',
      eventId: 'a-3',
      eventType: 'AccountCreated',
      eventNumber: 1,
      created: '13/04/2020 11:53:55',
      data: {}
    },
    {
      streamId: '1',
      eventId: 'b-2',
      eventType: 'AccountCreated',
      eventNumber: 1,
      created: '13/04/2020 11:53:56',
      data: {}
    },
    {
      streamId: '1',
      eventId: 'a-4',
      eventType: 'AccountCreated',
      eventNumber: 1,
      created: '13/04/2020 11:53:59',
      data: {}
    }
  ];
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        GetEventsFromAllAccountsStreamHandler,
        {
          provide: TCPClientToken,
          useValue: {
            getAllStreamEvents: jest.fn(() => testEvents),
            checkStreamExists: jest.fn(() => true).mockReturnValueOnce(false)
          }
        }
      ]
    }).compile();
    queryBus = module.get(QueryBus);
    queryBus.register([GetEventsFromAllAccountsStreamHandler]);
    esClient = module.get(TCPClientToken);
  });
  it('should throw all accounts stream not exist if the stream is not found', () => {
    expect(
      queryBus.execute(new GetEventsFromAllAccountsStreamQuery(100, 0))
    ).rejects.toThrowError('$ce-account not found');
  });
  it('should return an empty array if no events are present', () => {
    esClient.getAllStreamEvents = jest.fn().mockReturnValueOnce([]);
    expect(
      queryBus.execute(new GetEventsFromAllAccountsStreamQuery(100, 0))
    ).resolves.toHaveLength(0);
  });
  it('should return all required events', () => {
    esClient.getAllStreamEvents = jest.fn().mockReturnValueOnce(testEvents);
    expect(
      queryBus.execute(new GetEventsFromAllAccountsStreamQuery(100, 0))
    ).resolves.toBe(testEvents);
  });
  it('should return only the requested events', () => {
    esClient.getAllStreamEvents = jest.fn().mockReturnValueOnce(testEvents);
    const esSpy = jest.spyOn(esClient, 'getAllStreamEvents');
    queryBus.execute(new GetEventsFromAllAccountsStreamQuery(5, 0));
    expect(esSpy).toBeCalledWith('$ce-account', 5, 0);
  });
});
