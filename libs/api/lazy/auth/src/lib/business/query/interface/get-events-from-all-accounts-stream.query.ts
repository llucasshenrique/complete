export class GetEventsFromAllAccountsStreamQuery {
  constructor(
    public readonly quantity: number,
    public readonly offset: number,
    public readonly filter?: Array<string>
  ) {}
}
