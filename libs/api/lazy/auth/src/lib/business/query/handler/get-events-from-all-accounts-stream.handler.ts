import { Inject } from '@nestjs/common';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { TCPClient } from 'geteventstore-promise';
import { TCPClientToken } from '../../../auth-app.config';
import { GetEventsFromAllAccountsStreamQuery } from '../interface/get-events-from-all-accounts-stream.query';

@QueryHandler(GetEventsFromAllAccountsStreamQuery)
export class GetEventsFromAllAccountsStreamHandler
  implements IQueryHandler<GetEventsFromAllAccountsStreamQuery> {
  constructor(
    @Inject(TCPClientToken)
    private eventstore: TCPClient
  ) {}
  async execute({
    offset,
    quantity,
    filter
  }: GetEventsFromAllAccountsStreamQuery): Promise<any> {
    if(!this.eventstore.checkStreamExists('$ce-account')) {
      throw new Error('$ce-account not found')
    }
    return this.eventstore.getAllStreamEvents('$ce-account', quantity, offset);
  }
}
