import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      [
        {
          path: 'authorization',
          loadChildren: () =>
            import('./authorization/authorization.module').then(
              m => m.AuthorizationModule
            )
        },
        {
          path: '**',
          redirectTo: 'authorization'
        }
      ],
      { initialNavigation: 'enabled' }
    ),
    ButtonsModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
