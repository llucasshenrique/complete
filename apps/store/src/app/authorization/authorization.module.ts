import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { AuthorizationRoutingModule } from './authorization-routing.module';
import { AuthorizationComponent } from './authorization.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
  { path: '', component: AuthorizationComponent }
];

@NgModule({
  declarations: [AuthorizationComponent, RegisterComponent, LoginComponent],
  imports: [
    CommonModule,
    AuthorizationRoutingModule,
    RouterModule.forChild(routes)
  ]
})
export class AuthorizationModule { }
