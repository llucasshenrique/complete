import { AccountConnection, AccountEntity, AuthModule, EventEntity } from '@complete/api/lazy/auth';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { environment } from '../environments/environment';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      name: AccountConnection,
      type: 'sqlite',
      database: ':memory:',
      entities: [AccountEntity, EventEntity],
      synchronize: true
    }),
    AuthModule.forRoot(environment)
  ]
})
export class AppModule {}
