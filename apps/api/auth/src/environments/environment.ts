import { AuthAppConfig } from '@complete/api/lazy/auth';
import { Transport } from '@nestjs/microservices';

export const environment: AuthAppConfig = {
  production: false,
  authJsonWebTokenSecredt: 'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e',
  microserviceConfig: {
    transport: Transport.TCP
  },
  tcpConfig: {
    hostname: '140.238.181.30',
    credentials: {
      username: 'admin',
      password: '$ecurePass'
    },
    port: 1113,
    poolOptions: {
      min: 0,
      max: 10
    }
  }
};
