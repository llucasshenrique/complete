import { AuthAppConfig } from '@complete/api/lazy/auth';
import { Transport } from '@nestjs/microservices';

export const environment: AuthAppConfig = {
  production: true,
  authJsonWebTokenSecredt: process.env.auth_token,
  microserviceConfig: {
    transport: Transport.TCP
  },
  tcpConfig: {
    hostname: '152.67.34.230',
    credentials: {
      username: 'admin',
      password: 'changeit'
    },
    port: 1113,
    poolOptions: {
      min: 0,
      max: 10
    }
  }
}