/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 **/

import { NestFactory } from '@nestjs/core';

import { AppModule } from './app/app.module';
import { Logger } from '@nestjs/common';
import { Transport } from '@nestjs/microservices';

async function bootstrap() {
  const app = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.TCP,
    // options: {
    //   host: process.env.auth_host || '127.0.0.1',
    //   port: process.env.auth_port as unknown as number || 500
    // }
  });

  await app.listen(() => {
    Logger.log('Running', 'AuthMicroservice')
  });
}

bootstrap();
    