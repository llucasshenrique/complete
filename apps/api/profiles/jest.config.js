module.exports = {
  name: 'api-profiles',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/apps/api/profiles'
};
