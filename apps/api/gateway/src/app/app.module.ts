import { AuthHTTPModule } from '@complete/api/lazy/auth';
import { AppointmentHTTPModule } from '@complete/api/lazy/appointment';
import { Module } from '@nestjs/common';

@Module({
  imports: [
    AppointmentHTTPModule,
    AuthHTTPModule
  ]
})
export class AppModule {}
