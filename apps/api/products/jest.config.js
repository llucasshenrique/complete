module.exports = {
  name: 'api-products',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/apps/api/products'
};
