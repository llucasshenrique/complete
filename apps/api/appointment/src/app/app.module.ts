import { AppointmentModule } from '@complete/api/lazy/appointment';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      name: 'AppointmentConnection',
      type: 'sqlite',
      database: ':memory:',
      entities: [],
      synchronize: true
    }),
    AppointmentModule
  ]
})
export class AppModule {}
